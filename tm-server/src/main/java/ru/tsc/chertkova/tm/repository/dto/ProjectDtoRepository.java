package ru.tsc.chertkova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.tsc.chertkova.tm.dto.model.ProjectDTO;

import java.util.List;

@Repository
public interface ProjectDtoRepository extends AbstractUserOwnerModelDtoRepository<ProjectDTO> {

    @Modifying
    @Query("DELETE FROM ProjectDTO e WHERE e.userId=:userId")
    void clear(@NotNull @Param("userId") String userId);

    @NotNull
    @Query("SELECT e FROM ProjectDTO e WHERE e.userId=:userId")
    List<ProjectDTO> findAll(@NotNull @Param("userId") String userId);

    @Nullable
    @Query("SELECT e FROM ProjectDTO e WHERE e.id=:id AND e.userId=:userId")
    ProjectDTO findById(@NotNull @Param("userId") String userId,
                        @NotNull @Param("id") String id);

    @Query("SELECT COUNT(e) FROM ProjectDTO e WHERE e.userId=:userId")
    int getSize(@NotNull @Param("userId") String userId);

    @Modifying
    @Query("DELETE FROM ProjectDTO e WHERE e.id=:id AND e.userId=:userId")
    void removeById(@NotNull @Param("userId") String userId,
                    @NotNull @Param("id") String id);

    @Modifying
    @Query("UPDATE ProjectDTO e SET e.status=:status WHERE e.userId=:userId AND e.id=:id")
    void changeStatus(@NotNull @Param("id") String id,
                      @NotNull @Param("userId") String userId,
                      @NotNull @Param("status") String status);

}
