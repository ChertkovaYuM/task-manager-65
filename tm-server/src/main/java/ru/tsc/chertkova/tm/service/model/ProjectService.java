package ru.tsc.chertkova.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.chertkova.tm.api.service.model.IProjectService;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.exception.entity.*;
import ru.tsc.chertkova.tm.exception.field.*;
import ru.tsc.chertkova.tm.model.Project;
import ru.tsc.chertkova.tm.model.Task;
import ru.tsc.chertkova.tm.repository.model.ProjectRepository;
import ru.tsc.chertkova.tm.repository.model.TaskRepository;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ProjectService extends AbstractUserOwnerService<Project> implements IProjectService {

    @NotNull
    @Autowired
    private ProjectRepository projectRepository;

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Project add(@Nullable final Project project) {
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        Optional.ofNullable(project.getUser()).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(project.getName()).orElseThrow(NameEmptyException::new);
        projectRepository.saveAndFlush(project);
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Project updateById(@Nullable final String id,
                              @Nullable final String userId,
                              @Nullable final String name,
                              @Nullable final String description
    ) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        @Nullable Project project = Optional.ofNullable(findById(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        projectRepository.saveAndFlush(project);
        project = findById(userId, id);
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Project changeProjectStatusById(@Nullable final String userId,
                                           @Nullable final String id,
                                           @Nullable final Status status) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(status).orElseThrow(StatusNotFoundException::new);
        projectRepository.changeStatus(id, userId, status.getDisplayName());
        @Nullable Project project = findById(userId, id);
        return project;
    }

    @Override
    @Transactional
    public boolean existsById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        return projectRepository.existsById(id);
    }

    @Nullable
    @Override
    @Transactional
    public Project findById(@Nullable final String userId,
                            @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @Nullable Project project = projectRepository.findById(userId, id);
        return project;
    }

    @Override
    @SneakyThrows
    @Transactional
    public Project removeById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final Project project = Optional.ofNullable(findById(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        projectRepository.removeById(userId, id);
        return project;
    }

    @Override
    @SneakyThrows
    @Transactional
    public Project remove(@Nullable final String userId,
                          @Nullable final Project project) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        Optional.ofNullable(findById(project.getUser().getId(), project.getId()))
                .orElseThrow(ProjectNotFoundException::new);
        removeById(project.getUser().getId(), project.getId());
        return project;
    }

    @Override
    @SneakyThrows
    @Transactional
    public int getSize(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        int size = projectRepository.getSize(userId);
        return size;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        projectRepository.clear(userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public List<Project> findAll(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        @Nullable List<Project> projects = projectRepository.findAll(userId);
        return projects;
    }

    @Nullable
    @Override
    @Transactional
    public List<Project> addAll(@NotNull final List<Project> projects) {
        Optional.ofNullable(projects).orElseThrow(ProjectNotFoundException::new);
        projectRepository.saveAll(projects);
        return projects;
    }

    @Nullable
    @Override
    @Transactional
    public List<Project> removeAll(@Nullable final List<Project> projects) {
        Optional.ofNullable(projects).orElseThrow(ProjectNotFoundException::new);
        projectRepository.deleteAll(projects);
        return projects;
    }

    @Override
    @SneakyThrows
    @Transactional
    public Task bindTaskToProject(@Nullable final String userId,
                                  @Nullable final String projectId,
                                  @Nullable final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new IdEmptyException();
        @NotNull final TaskRepository taskRepository = context.getBean(TaskRepository.class);
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        Optional.ofNullable(taskRepository.findById(userId, taskId)).orElseThrow(TaskNotFoundException::new);
        taskRepository.bindTaskToProject(taskId, projectId, userId);
        @Nullable Task task = taskRepository.findById(userId, taskId);
        return task;
    }

}
