package ru.tsc.chertkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.chertkova.tm.api.component.ISaltProvider;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorName();

    @NotNull
    String getAuthorEmail();

    @NotNull
    Integer getServerPort();

    @NotNull
    String getServerHost();

    @NotNull
    String getSessionKey();

    @NotNull
    Integer getSessionTimeout();

    @NotNull
    String getDatabaseUsername();

    @NotNull
    String getDatabasePassword();

    @NotNull
    String getDatabaseUrl();

    @NotNull
    String getDatabaseDriver();

    @NotNull
    String getDatabaseDialect();

    @NotNull
    String getDatabaseHBM2DDL();

    @NotNull
    String getDatabaseShowSQL();

    @NotNull
    String getDatabaseUseSecondLvlCache();

    @NotNull
    String getDatabaseUseQueryCache();

    @NotNull
    String getDatabaseUseMinimalPuts();

    @NotNull
    String getDatabaseCacheRegionPrefix();

    @NotNull
    String getDatabaseCacheConfigFile();

    @NotNull
    String getDatabaseCacheFactoryClass();

}
