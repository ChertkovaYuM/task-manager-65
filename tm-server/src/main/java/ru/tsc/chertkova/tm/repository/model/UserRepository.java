package ru.tsc.chertkova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.tsc.chertkova.tm.enumerated.Role;
import ru.tsc.chertkova.tm.model.User;

@Repository
public interface UserRepository extends AbstractRepository<User> {

    @Nullable
    @Query("SELECT e FROM User e WHERE e.email=:email")
    User findByEmail(@NotNull @Param("email") String email);

    @Nullable
    @Query("SELECT e FROM User e WHERE e.login=:login")
    User findByLogin(@NotNull @Param("login") String login);

    @Modifying
    @Query("DELETE FROM User e WHERE e.login=:login")
    void removeByLogin(@NotNull @Param("login") String login);

    @Query("SELECT COUNT(e) FROM User e WHERE e.login=:login")
    long isLoginExist(@NotNull @Param("login") String login);

    @Query("SELECT COUNT(e) FROM User e WHERE e.email=:email")
    long isEmailExist(@NotNull @Param("email") String email);

    @Modifying
    @Query("UPDATE User e SET e.role=:role WHERE e.id=:id")
    void changeRole(@NotNull @Param("id") String id,
                    @NotNull @Param("role") Role role);

    @Modifying
    @Query("UPDATE User e SET e.passwordHash=:passwordHash WHERE e.id=:id")
    void setPassword(@NotNull @Param("id") String id,
                     @NotNull @Param("passwordHash") String passwordHash);

    @Modifying
    @Query("UPDATE User e SET e.locked=:locked WHERE e.login=:login")
    void setLockedFlag(@NotNull @Param("login") String login,
                       @NotNull @Param("locked") Boolean locked);

}
