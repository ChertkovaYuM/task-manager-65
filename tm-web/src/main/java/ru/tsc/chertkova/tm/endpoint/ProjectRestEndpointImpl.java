package ru.tsc.chertkova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.chertkova.tm.api.ProjectRestEndpoint;
import ru.tsc.chertkova.tm.model.Project;
import ru.tsc.chertkova.tm.repository.ProjectRepository;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/project")
public class ProjectRestEndpointImpl implements ProjectRestEndpoint {

    @NotNull
    @Autowired
    private ProjectRepository projectRepository;

    @Override
    @GetMapping("/findAll")
    public List<Project> findAll() {
        return projectRepository
                .findAll()
                .stream()
                .collect(Collectors.toList());
    }

    @Override
    @GetMapping("/findById/{id}")
    public Project findById(@NotNull @PathVariable("id") final String id) {
        return projectRepository.findById(id);
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@NotNull @PathVariable("id") final String id) {
        return projectRepository.existsById(id);
    }

    @Override
    @PostMapping("/save")
    public Project save(@NotNull @RequestBody final Project project) {
        return projectRepository.add(project);
    }

    @Override
    @PostMapping("/delete")
    public void delete(@NotNull @RequestBody final Project project) {
        projectRepository.remove(project);
    }

    @Override
    @PostMapping("/deleteAll")
    public void clear(@NotNull @RequestBody final List<Project> projects) {
        projectRepository.remove(projects);
    }

    @Override
    @DeleteMapping("/clear")
    public void clear() {
        projectRepository.clear();
    }

    @Override
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@NotNull final String id) {
        projectRepository.removeById(id);
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return projectRepository.count();
    }

}
