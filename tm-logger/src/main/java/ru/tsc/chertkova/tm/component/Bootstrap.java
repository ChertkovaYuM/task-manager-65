package ru.tsc.chertkova.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.api.service.IReceiverService;
import ru.tsc.chertkova.tm.listener.LoggerListener;

@Component
public final class Bootstrap {

    @Nullable
    @Autowired
    private IReceiverService receiverService;

    @SneakyThrows
    public void init() {
        receiverService.receive(new LoggerListener());
    }

}
